<?php
?>
  <div class="comment<?php print ' '. $status; ?>">
    <?php if ($picture) { print '<div class="picture">' .l(theme('imagecache','50x50',$comment->picture,'','',array()),'user/'.$comment->name,array('html' => true, 'attributes' => array('title' => "See ".$comment->name."'s profile"))). '</div>'; } ?>
	<div class="content">
      <h3 class="title"><?php print $comment->subject; ?></h3><?php if ($new != '') { ?><span class="new"><?php print $new; ?></span><?php } ?>
      <?php print $content; ?>
	  <?php if ($signature): ?><div class="clear-block"><div>—</div><?php print $signature ?></div><?php endif; ?>
	  <div class="links">
	    <div class="submitted"><?php print 'Posted by ' .$comment->name. ' on ' .date('j M, Y h:ia',$comment->timestamp); ?></div>
	    <?php print $links; ?>
	  </div>
    </div>
  </div>