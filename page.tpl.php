<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <?php print $head ?>
  <title><?php print $head_title ?></title>
  <?php print $styles ?>
  <?php print $scripts ?>
</head>

<body>

  <div class="mholder">
    
	<div class="bgholder">
	
      <div id="header">
	    <div class="header-content"><?php if ($header) { print $header;} ?></div>
		<?php if ($logo) { print '<div id="logo"><a href="' .url(). '"><img src="' .$logo. '" alt="Home" /></a></div>'; } ?>
	    <?php if ($site_name) { print '<h1 class="site-name">' .l($site_name, '', array('attributes' => array('title' => t('Home')))). '</h1>'; } ?>
	    <?php if ($site_slogan) { ?><div class="site-slogan"><?php print $site_slogan; ?></div><?php } ?>
		<div id="menu">
          <?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' => 'links', 'id' => 'navlist')) ?><?php } ?>
          <?php print $search_box ?>
        </div>
	  </div>
	  
	  <?php 
		if (drupal_is_front_page()) {
		  $slider = db_query("SELECT DISTINCT n.nid, n.title, f.filepath, ctn.field_news_brief_value FROM {node} n INNER JOIN {content_type_news} ctn ON ctn.nid = n.nid INNER JOIN {files} f ON f.fid = ctn.field_news_image_fid WHERE n.type = '%s' AND n.status = %d AND ctn.field_news_featured_value = '%s' ORDER BY n.created DESC LIMIT %d", 'news', 1, 'yes', 4);
		  $slider_output = ''; $controls_output = ''; $slider_index = 0;
		  while ($slider_item = db_fetch_object($slider)) {
		    $slider_index ++;
			$controls_output .= '<a id="slider-item-' .$slider_index. '"></a>';
		    $slider_output .= '<div class="slider-item slider-item-' .$slider_index. '">';
			$slider_output .= l(theme('imagecache','396x220',$slider_item->filepath,'','',array()),'node/'.$slider_item->nid,array('html' => true));
			$slider_output .= '<span class="title">' .$slider_item->title. '</span>';
			$slider_output .= '<p>' .substr($slider_item->field_news_brief_value,0,300). '</p>';
			$slider_output .= l(t('Read more'),'node/'.$slider_item->nid,array('attributes' => array('class' => 'readmore')));
			$slider_output .= '</div>';
		  }
		  if ($slider_output != '') { print '<div id="slider">' .$slider_output. '<div id="controls">' .$controls_output. '</div></div>';}
		  if ($mission) { print '<div id="mission">' .$mission. '</div>'; }
		}
	  ?>
	  
	  <?php if ($precontent) { print '<div id="precontent">' .$precontent. '</div>'; } ?>
	  
	  <div id="content">
	    <?php 
		  if (drupal_is_front_page()) {
		    print '<div class="category-items">';
		    $cat_terms = db_query("SELECT DISTINCT td.tid, td.name FROM {term_data} td INNER JOIN {term_node} tn ON tn.tid = td.tid WHERE td.vid = %d", 1);
			while ($cat_term = db_fetch_object($cat_terms)) {
			  $last_node = db_fetch_object(db_query("SELECT n.nid, n.title, f.filepath, ctn.field_news_brief_value FROM {node} n INNER JOIN {term_node} tn ON tn.nid = n.nid INNER JOIN {content_type_news} ctn ON ctn.nid = n.nid INNER JOIN {files} f ON f.fid = ctn.field_news_image_fid WHERE n.type = '%s' AND n.status = %d AND tn.tid = %d ORDER BY n.created DESC LIMIT %d", 'news', 1, $cat_term->tid, 1));
			  print '<div class="category">';
			  print '<div class="cat-name">' .$cat_term->name. '&nbsp;&raquo;</div>';
			  print '<div class="cat-readmore">' .l(t('Read more in this category'), 'taxonomy/term/'.$cat_term->tid). '</div>';
			  print '<div class="cat-last">';
			  print l(theme('imagecache', '140x110', $last_node->filepath, '', '', array()),'node/'.$last_node->nid,array('html' => true));
			  print '<h2>' .l($last_node->title, 'node/'.$last_node->nid). '</h2>';
			  print '<p>' .substr($last_node->field_news_brief_value, 0, 150). '...</p>';
			  print '</div>';
			  print '</div>';
			}
			print '</div>';
		  }
		?>
	    <?php if (!drupal_is_front_page()) { ?>
	      <?php if ($right) { ?><div id="main"><?php } else { ?><div id="main" class="mainadmin"><?php } ?>
            <?php if (arg(0) == 'taxonomy' && arg(1) == 'term') { ?><h1 class="title"><?php print $title.'&nbsp;&raquo;'; ?></h1><?php } else { ?><h1 class="title"><?php print $title; ?></h1><?php } ?>
            <div class="tabs"><?php print $tabs ?></div>
            <?php if ($show_messages) { print $messages; } ?>
            <?php print $help ?>
            <?php print $content; ?>
            <?php print $feed_icons; ?>
          </div>
		  <?php if ($right) { ?><div id="mright"><?php print $right; ?></div><? } ?>
		<?php } ?>
	  </div>
	  
	  <?php if ($aftercontent) { print '<div id="aftercontent">' .$aftercontent. '</div>'; } ?>
	  
	  <?php if (isset($secondary_links)) { ?><?php print theme('links', $secondary_links, array('class' => 'links', 'id' => 'subnavlist')); ?><?php } ?>
	  <div id="copy"><?php print $footer_message ?></div>
	
	</div>
	
	<div id="footer"><div class="copyright"><strong><a href="http://www.adciserver.com" title="Go to adciserver.com">Drupal theme</a></strong> by <a href="http://www.adciserver.com" title="Go to adciserver.com">www.adciserver.com</a></div></div>
	
  </div>

<?php print $closure ?>
</body>
</html>
